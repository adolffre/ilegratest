//
//  Supporter.swift
//  IlegraApp
//
//  Created by A. J. on 02/07/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

import Foundation

class Supporter {
    var code             :Int!    = nil
    var name             :String! = nil
    var registrationId   :String! = nil
    var overdue          :Bool!
    var codTeam          :Int!    = nil
    
    
    init() {
        Supporter.createTable()
    }
    
    static func createTable() -> Bool {
        let (tb, err) = SD.existingTables()
        if !contains(tb, "Supporter") {
            if let err = SD.createTable("Supporter", withColumnNamesAndTypes:
                [     "code"             : .IntVal
                    , "name"             : .StringVal
                    , "registrationId"   : .StringVal
                    , "overdue"          : .BoolVal
                    , "codTeam"          : .IntVal
                ]) {
                    //there was an error during this function, handle it here
                    return false
            } else {
                //no error, the table was created successfully
                Supporter.initializeTests()
                return true
            }
        }
        return true
    }
    static func initializeTests(){
        for index in 1...10
        {
            var sup  = Supporter()
            sup.name = "Supporter\(index)"
            sup.registrationId  = "\(index*1238)"
            sup.overdue = false
            var team: Int = random() % 3 + 1
            sup.codTeam = team
            Supporter.add(sup)
        }
    }

    static func add(supporter :Supporter) -> Int{
        var result: Int? = nil
        if let err = SD.executeChange(
            "INSERT INTO SUPPORTER (code, name , registrationId, overdue, codTeam) VALUES (?,?,?,?,?) ",
            withArgs:
            [     getNewSequence()
                , supporter.name
                , supporter.registrationId
                , supporter.overdue
                , supporter.codTeam
            ])
        {
            //there was an error during the insert, handle it here
        } else {
            //no error, the row was inserted successfully
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                //err
            }else{
                //ok
                result = Int(id)
            }
        }
        return result!
    }
    static func getAllSupportersFrom(team : Team) -> NSArray {
        var result = NSMutableArray()
        
        let (resultSet, err) = SD.executeQuery("SELECT * FROM SUPPORTER WHERE codTeam = ?", withArgs:[team.code])
        if err != nil {
            
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let sup = Supporter()
                    sup.code            = row["code"]?.asInt()!
                    sup.name            = row["name"]?.asString()!
                    sup.registrationId  = row["registrationId"]?.asString()!
                    sup.overdue         = row["overdue"]!.asBool()!
                    sup.codTeam         = row["codTeam"]!.asInt()!

                    result.addObject(sup)
                }
            }
        }
        return result
    }

    static func getNewSequence() -> Int {
        var result:Int! = 1
        let (resultSet, err) = SD.executeQuery("SELECT IFNULL(MAX(code), 0)+1 AS code FROM SUPPORTER  ")
        
        if err != nil {
            
        } else {
            for row in resultSet {
                if let id = row["code"]?.asInt() {
                    result = row["code"]?.asInt()!
                }
            }
        }
        return result
        
    }
}
