//
//  Player.swift
//  IlegraApp
//
//  Created by A. J. on 02/07/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

import Foundation

class Player {
    var code         :Int!     = nil
    var name         :String!  = nil
    var age          :Int!     = nil
    var salary       :Float!   = nil
    var codTeam      :Int!     = nil
    
    init() {
        Player.createTable()
    }
    
    static func createTable() -> Bool {
        let (tb, err) = SD.existingTables()
        if !contains(tb, "Player") {
            if let err = SD.createTable("Player", withColumnNamesAndTypes:
                [     "code"    : .IntVal
                    , "name"    : .StringVal
                    , "age"     : .IntVal
                    , "salary"  : .DoubleVal
                    , "codTeam" : .IntVal
                ]) {
                    //there was an error during this function, handle it here
                    return false
            } else {
                //no error, the table was created successfully
                Player.initializeTests()
                return true
            }
            
        }
        
        return true
    }
    static func initializeTests(){
        var i = 1
        for index in 1...30
        {
            var player  = Player()
            player.name = "Player\(index)"
            player.age  = index+14
            player.salary = Float(750*index)
            if i > 3
            {
                i=1
            }
            player.codTeam = i
            i++
            Player.add(player)
        }
    }
    static func add(player :Player) -> Int{
        var result: Int? = nil
        if let err = SD.executeChange(
            "INSERT INTO PLAYER (code, name , age, salary, codTeam) VALUES (?,?,?,?,?) ",
            withArgs:
            [     getNewSequence()
                , player.name
                , player.age
                , player.salary
                , player.codTeam
            ])
        {
            //there was an error during the insert, handle it here
        } else {
            //no error, the row was inserted successfully
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                //err
            }else{
                //ok
                result = Int(id)
            }
        }
        return result!
    }
    static func getNewSequence() -> Int {
        var result:Int! = 1
        let (resultSet, err) = SD.executeQuery("SELECT IFNULL(MAX(code), 0)+1 AS code FROM PLAYER  ")
        
        if err != nil {
            
        } else {
            for row in resultSet {
                if let id = row["code"]?.asInt() {
                    result = row["code"]?.asInt()!
                }
            }
        }
        return result
        
    }
    static func getAllPlayerFrom(team : Team) -> NSArray {
            var result = NSMutableArray()

            let (resultSet, err) = SD.executeQuery("SELECT * FROM PLAYER WHERE codTeam = ?", withArgs:[team.code])
            if err != nil {
    
            } else {
                for row in resultSet {
                    if let id = row["ID"]?.asInt() {
                        let pl = Player()
                        pl.code = row["code"]?.asInt()!
                        pl.name = row["name"]?.asString()!
                        var sal = row["salary"]?.asDouble()!
                        pl.salary = Float(sal!)
                        pl.age = row["age"]!.asInt()!
                        pl.codTeam = row["codTeam"]!.asInt()!
                        result.addObject(pl)
                    }
                }
            }
            return result
        }
    

}
