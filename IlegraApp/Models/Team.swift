//
//  Team.swift
//  IlegraApp
//
//  Created by A. J. on 02/07/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

import Foundation

class Team {
    var code      :Int!     = nil
    var name      :String!  = nil
    var players   :NSArray! = nil
    var supporters:NSArray! = nil

    init() {
        Team.createTable()
    }
    
    static func createTable() -> Bool {
        let (tb, err) = SD.existingTables()
        if !contains(tb, "Team") {
            if let err = SD.createTable("Team", withColumnNamesAndTypes:
                [     "code"   : .IntVal
                    , "name"   : .StringVal
                ]) {
                    //there was an error during this function, handle it here
                    return false
            } else {
                //no error, the table was created successfully
                Team.initializeTests()
                return true
            }
        }
        return true
    }
    static func initializeTests(){
        for index in 1...3
        {
            var tea  = Team()

            tea.name  = "Team\(index)"

            Team.add(tea)
        }
    }
    
    static func add(team :Team) -> Int{
        var result: Int? = nil
        if let err = SD.executeChange(
            "INSERT INTO TEAM (code, name ) VALUES (?,?) ",
            withArgs:
            [     getNewSequence()
                , team.name
            ])
        {
            //there was an error during the insert, handle it here
        } else {
            //no error, the row was inserted successfully
            let (id, err) = SD.lastInsertedRowID()
            if err != nil {
                //err
            }else{
                //ok
                result = Int(id)
            }
        }
        return result!
    }

    
    
    static func getNewSequence() -> Int {
        var result:Int! = 1
        let (resultSet, err) = SD.executeQuery("SELECT IFNULL(MAX(code), 0)+1 AS code FROM TEAM  ")
        
        if err != nil {
            
        } else {
            for row in resultSet {
                if let id = row["code"]?.asInt() {
                    result = row["code"]?.asInt()!
                }
            }
        }
        return result
        
    }
    
    static func getAll() -> NSArray {
        var result = NSMutableArray()
        let (resultSet, err) = SD.executeQuery(
            " SELECT t.ID, t.code, " +
            "        t.NAME, " +
            "        Sum(p.salary) " +
            " FROM   player p " +
            "        INNER JOIN team t " +
            "                ON t.code = p.codteam " +
            "        LEFT JOIN (SELECT codteam, " +
            "                          Count(*) AS qtd " +
            "                   FROM   supporter s " +
            "                   GROUP  BY s.codteam) s " +
            "               ON s.codteam = t.code " +
            " GROUP  BY p.codteam " +
            " ORDER  BY Sum(p.salary) DESC, " +
            "           s.qtd DESC " )
        if err != nil {
            
        } else {
            for row in resultSet {
                if let id = row["ID"]?.asInt() {
                    let team = Team()
                    team.code = row["code"]?.asInt()!
                    team.name = row["name"]?.asString()!
                    
                    result.addObject(team)
                }
            }
        }
        return result
    }
    
    
    
}
