//
//  TeamViewController.swift
//  IlegraApp
//
//  Created by A. J. on 02/07/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

import UIKit

class TeamViewController: UIViewController {

    var teams :NSArray!
    var selectedTeam :Team?{
        willSet (newTeam){
            self.selectedTeam = newTeam
            self.selectedTeam?.players = Player.getAllPlayerFrom(newTeam!)
            var a = 0
        }
        didSet {
            self.performSegueWithIdentifier("playersId", sender: self)
        }
        
    }
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Teams"
        
        teams = NSArray(array: Team.getAll())
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
            
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Tableview DataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teams.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("teamCellId", forIndexPath: indexPath) as! UITableViewCell
        
        var team : Team = self.teams[indexPath.row] as! Team
        
        var lblName    = cell.viewWithTag(1) as! UILabel
        
        lblName.text   = team.name as String
        
        return cell
        
    }
    //MARK: Tableview Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedTeam = self.teams.objectAtIndex(indexPath.row) as? Team
    }
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "playersId"{
            let vc = segue.destinationViewController as! PlayersViewController
            vc.selectedTeam = self.selectedTeam
        }
    }
}
