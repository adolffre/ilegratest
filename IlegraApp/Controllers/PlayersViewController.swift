//
//  PlayersViewController.swift
//  IlegraApp
//
//  Created by A. J. on 02/07/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController {
    var selectedTeam : Team!{
        willSet (newTeam){
            self.players      = newTeam.players
            self.selectedTeam = newTeam
            self.supporters    = Supporter.getAllSupportersFrom(newTeam)
            self.calcTotalSalaries()
        }
    }
    var players :NSArray!
    var supporters :NSArray!
    var totalSalaries :Float!
    @IBOutlet weak var lblTotalSalary: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblSupporters: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSString(format: "%@ Players", self.selectedTeam.name) as String
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.initializeView()
        tableView.reloadData()
        
    }
    
    //MARK: Custom Methods
    func initializeView(){
        self.lblTeamName.text    = self.selectedTeam.name as String
        self.lblSupporters.text  = NSString(format: "%i", self.supporters.count) as String
        self.lblTotalSalary.text = NSString(format: "$%.2f", self.totalSalaries) as String
        
    }
    func calcTotalSalaries()
    {
        var total :Float = 0
        for player in self.players as! [Player]
        {
            total += player.salary
        }
        self.totalSalaries = total
    }
    
    //MARK: Tableview DataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.players.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("playerCellId", forIndexPath: indexPath) as! UITableViewCell
        
        var player : Player = self.players[indexPath.row] as! Player
        
        var lblName    = cell.viewWithTag(1) as! UILabel
        var lblSalary  = cell.viewWithTag(2) as! UILabel
        
        lblName.text   = player.name as String
        lblSalary.text = NSString(format: "$%.2f", player.salary) as String
        
        return cell
        
    }
    //MARK: Tableview Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //        self.pedidoSelecionado = pedidosSelecionados.objectAtIndex(indexPath.row) as? Pedido
    }

}
